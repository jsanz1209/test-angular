export const environment = {
  production: true,
  API_URL: 'https://localhost:4000',
  deploymentId: '05b7f7ea-c292-456a-a7cf-10115f2fcb0d',
  API_WS_URL: 'wss://webmessaging.mypurecloud.com/v1?deploymentId='
};

