import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from 'src/app/core/modules/material.module';
import { ChatBotRoutingModule } from './chat-bot-routing/chat-bot-routing.module';
import { ChatBotComponent } from './chat-bot.component';
import { ChatBotService } from './services/chat-bot.service';
import { GroupBubbleChatComponent } from './components/group-bubble-chat/group-bubble-chat.component';
import { RatingComponent } from './components/rating/rating.component';
import { StarRatingComponent } from './components/star-rating/star-rating.component';

@NgModule({
    declarations: [
        ChatBotComponent,
        GroupBubbleChatComponent,
        RatingComponent,
        StarRatingComponent
    ],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        MaterialModule,
        ChatBotRoutingModule
    ],
    providers: [
        ChatBotService
    ]
  })
  export class ChatBotModule {}
