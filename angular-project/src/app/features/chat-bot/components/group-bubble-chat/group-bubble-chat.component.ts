import { Component, Input, OnChanges } from '@angular/core';
import { Message } from 'src/app/core/models/message.model';

@Component({
  selector: 'app-group-bubble-chat',
  templateUrl: './group-bubble-chat.component.html',
  styleUrls: ['./group-bubble-chat.component.scss']
})
export class GroupBubbleChatComponent implements OnChanges {

  @Input() messages: Message[] = [];
  currentDate: Date | undefined;

  constructor() { }

  ngOnChanges(): void {
    this.currentDate = new Date();
  }

}
