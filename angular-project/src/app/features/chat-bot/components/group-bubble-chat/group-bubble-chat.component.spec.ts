import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupBubbleChatComponent } from './group-bubble-chat.component';

describe('GroupBubbleChatComponent', () => {
  let component: GroupBubbleChatComponent;
  let fixture: ComponentFixture<GroupBubbleChatComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GroupBubbleChatComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupBubbleChatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
