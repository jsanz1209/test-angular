import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { MatRadioChange } from '@angular/material/radio';
import { Observable } from 'rxjs';
import { ChatBotService } from '../../services/chat-bot.service';
import { StarRatingColor } from '../star-rating/star-rating.component';

interface RatingType {
  question: string;
  type: 'stars' | 'radio' | 'text';
}

@Component({
  selector: 'app-rating',
  templateUrl: './rating.component.html',
  styleUrls: ['./rating.component.scss']
})
export class RatingComponent implements OnChanges {

  @Input() isChatRequestedByUser: boolean | undefined;
  @Output() closed = new EventEmitter();
  @Output() finished = new EventEmitter();

  currentPosition = 0;
  error = false;
  evaluationEnded = false;

  rating = 0;
  starCount = 5;
  starColor = StarRatingColor.accent;
  starColorP = StarRatingColor.primary;
  starColorW = StarRatingColor.warn;
  responseText: string | null = null;
  responseRadio: boolean | null = null;
  questions: RatingType[];

  private typeAgent = 'tabot';
  private evaluationID = '';
  private responses = {};

  private tabotQuestions: RatingType[] = [
    { question: '¿Qué tan satisfactoria fue la experiencia con Tabot, el asistente virtual?', type: 'stars' },
    { question: '¿Lograste resolver tu  solicitud con Tabot, el asistente virtual?', type: 'radio' },
    { question: '¿Qué tan fácil fue resolver tu solicitud con Tabot, el asistente virtual?', type: 'stars' },
    { question: '¿En qué puede mejorar Tabot, el asistente virtual? ', type: 'text' }
  ];

  private agentQuestions: RatingType[] = [
    { question: '¿Qué tan satisfactoria fue la experiencia con el asesor?', type: 'stars' },
    { question: '¿Lograste resolver tu  solicitud con el asesor?', type: 'radio' },
    { question: '¿Qué tan fácil fue resolver tu solicitud con el asesor?', type: 'stars' },
    { question: '¿En qué puede mejorar  el asesor? ', type: 'text' }
  ];

  constructor(private chatBotSrv: ChatBotService) {
    this.questions = this.tabotQuestions;
  }

  ngOnChanges(): void {
    if (this.isChatRequestedByUser) {
      this.currentPosition = -1;
    } else {
      this.generateEvaluation(this.typeAgent);
    }
  }

  onChangeAgentToEvaluate(event: MatRadioChange): void {
    console.log(event);
    if (event.value === 'agent') {
      this.typeAgent = 'Agent';
      this.questions = this.agentQuestions;
    } else {
      this.typeAgent = 'Tabot';
      this.questions = this.tabotQuestions;
    }
  }

  onChange(): void {
    this.error = false;
  }

  onNext(): void {
    if (this.responseText === null && this.responseRadio === null && this.rating === 0 && this.currentPosition !== -1) {
      this.error = true;
    } else if (this.currentPosition === -1) {
        this.generateEvaluation(this.typeAgent);
        this.currentPosition++;
    } else {
      const type = this.questions[this.currentPosition].type;
      const response =  type === 'radio' ? this.responseRadio : type === 'text' ? this.responseText : this.rating;
      this.currentPosition++;
      this.addEvaluation(this.currentPosition, response);
      this.error = false;
      this.rating = 0;
      this.responseText = null;
      this.responseRadio = null;

      if (this.currentPosition === this.questions.length) {
        this.evaluationEnded = true;
        this.finished.emit();
      }
    }
  }

  onRatingChanged(rating: number): void {
    this.error = false;
    this.rating = rating;
  }

  onClosed(): void{
    this.closed.emit();
  }

  private generateEvaluation(typeAgent: string): void {
    this.chatBotSrv.generateEvaluation(this.typeAgent).subscribe(({evaluationID}) => {
      this.evaluationID = evaluationID;
    });
  }

  private addEvaluation(index: number, response: string | boolean | number | null): void {
    this.responses = {
      ...this.responses,
      [`response${index}`]: response
    };
    this.chatBotSrv.addEvaluation(this.evaluationID, this.responses).subscribe();
  }
}
