import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-star-rating',
  templateUrl: './star-rating.component.html',
  styleUrls: ['./star-rating.component.scss']
})
export class StarRatingComponent implements OnInit {

  @Input() rating = 3;
  @Input() starCount = 5;
  @Output() private ratingUpdated = new EventEmitter();

  ratingArr: number[] = [];

  ngOnInit(): void {
    for (let index = 0; index < this.starCount; index++) {
      this.ratingArr.push(index);
    }
  }
  onClick(rating: number): void {
    this.ratingUpdated.emit(rating);
  }

  showIcon(index: number): string {
    if (this.rating >= index + 1) {
      return 'star';
    } else {
      return 'star_border';
    }
  }

  getSatisfaction(rating: number): string {
    switch (rating) {
      case 1:
        return 'Nada satisfactoria';

      case 2:
        return 'Poco satisfactoria';

      case 3:
        return 'Neutral';

      case 4:
        return 'Satisfactoria';

      case 5:
        return 'Muy satisfactoria';

      default:
        return '';
    }
  }
}
export enum StarRatingColor {
  primary = 'primary',
  accent = 'accent',
  warn = 'warn'
}
