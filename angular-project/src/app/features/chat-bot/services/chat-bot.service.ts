import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Message } from 'src/app/core/models/message.model';
import { StorageService } from 'src/app/core/services/storage.service';
import { environment } from 'src/environments/environment';

const tokenAuthTLS = 'Q0xSOkNMUjIwMTkq';

@Injectable({
  providedIn: 'root'
})
export class ChatBotService {

  constructor(private httpClient: HttpClient, private storageSrv: StorageService) { }

  authenticate(): Observable<any> {
    const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json', Authorization: `Basic ${tokenAuthTLS}` }) };
    return this.httpClient.post<any>(`${environment.API_URL}/tls/authenticate`, {}, httpOptions)
      .pipe(
        tap(res => {
          this.storageSrv.setSessionStorage('chatID', res.chatID);
          this.storageSrv.setSessionStorage('userID', res.userID);
          this.storageSrv.setSessionStorage('accessToken', res.accessToken);
        })
      );
  }

  checkHealthBot(): Observable<any> {
    const httpOptions = this.getAuthorizationHeader();
    return this.httpClient.get<any>(`${environment.API_URL}/tls/check-health-bot`, httpOptions);
  }

  disconnect(): Observable<any> {
    const httpOptions = this.getAuthorizationHeader();
    const chatID = this.storageSrv.getSessionStorage('chatID');
    const userID = this.storageSrv.getSessionStorage('userID');
    return this.httpClient.post<any>(`${environment.API_URL}/tls/disconnect`, {
      chatID,
      userID
    }, httpOptions);
  }

  getInitialConfiguration(): Observable<any> {
    const httpOptions = this.getAuthorizationHeader();
    return this.httpClient.get<any>(`${environment.API_URL}/tls/get-bot-configuration`, httpOptions);
  }

  readWriteBot(input: string[]): Observable<any> {
    const httpOptions = this.getAuthorizationHeader();
    return this.httpClient.post<any>(`${environment.API_URL}/tls/read-write-bot`, {
      arrayLevels: input,
    }, httpOptions);
  }

  generateEvaluation(typeAgent: string): Observable<any> {
    const httpOptions = this.getAuthorizationHeader();
    const chatID = this.storageSrv.getSessionStorage('chatID');
    const userID = this.storageSrv.getSessionStorage('userID');
    const personalData = this.storageSrv.getSessionStorage('personalData');
    return this.httpClient.post<any>(`${environment.API_URL}/tls/generate-evaluation`, {
      chatID,
      userID,
      typeAgent,
      personalData: JSON.parse(personalData),
    }, httpOptions);
  }

addEvaluation(evaluationID: string, responses: any): Observable<any> {
    const httpOptions = this.getAuthorizationHeader();
    return this.httpClient.post<any>(`${environment.API_URL}/tls/add-evaluation`, {
      evaluationID,
      responses,
    }, httpOptions);
  }

  sendEmail(messages: Message[]): Observable<any> {
    return this.httpClient.post<any>(`${environment.API_URL}/send-email`, {
      messages
    });
  }

  private getAuthorizationHeader(): {
    headers: HttpHeaders;
  } {
    const accessToken = this.storageSrv.getSessionStorage('accessToken');
    const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json', Authorization: `Bearer ${accessToken}` }) };
    return httpOptions;
  }
}
