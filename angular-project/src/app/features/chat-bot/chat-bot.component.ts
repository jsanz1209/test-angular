import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { EMPTY, Observable, Subject } from 'rxjs';
import { mergeMap, take, takeUntil } from 'rxjs/operators';
import { Message } from 'src/app/core/models/message.model';
import { StorageService } from 'src/app/core/services/storage.service';
import { WebsocketService } from 'src/app/core/services/websocket.service';
import { ChatBotService } from './services/chat-bot.service';

@Component({
  selector: 'app-chat-bot',
  templateUrl: './chat-bot.component.html',
  styleUrls: ['./chat-bot.component.scss'],
  providers: [WebsocketService]
})
export class ChatBotComponent implements OnInit, OnDestroy {
  content = '';

  // BOT
  sentBot: string[] = [];
  messages: Message[] = [];

  checkSendEmail = false;
  chatEnded = false;
  isBotMode = true;
  tabotIsConnected = false;
  showEvaluation = false;
  evaluationEnded = false;
  isConnectedWS = false;

  private destroyed$ = new Subject();

  constructor(private chatBotSrv: ChatBotService,
    private websocketService: WebsocketService,
    private snackBar: MatSnackBar,
    private storageSrv: StorageService,
    private router: Router) { }

  ngOnInit(): void {
    this.connectWS();
  }

  ngOnDestroy(): void {
    this.closeChat();
    this.destroyed$.next();
    this.destroyed$.complete();
  }

  onSendMessage(): void {
    if (this.content.toLowerCase() === 'asesor') {
      this.connectWS();
      return;
    }
    if (this.isBotMode) {
      this.sendMessageBotMode();
    } else {
      this.sendMessagegWebSocket();
    }
  }

  onCloseChat(): void {
    this.closeChat();
  }

  onEvaluationClosed(): void {
    this.showEvaluation = false;
  }

  onEvaluationFinished(): void {
    this.evaluationEnded = true;
  }

  onEvaluatePressed(): void {
    this.showEvaluation = true;
  }

  private connectWS(): void {
    this.isBotMode = false;
    this.websocketService.requestConnection();
    this.websocketService.connectionEstablished?.pipe(takeUntil(this.destroyed$))
      .subscribe(() => {
        this.tabotIsConnected = false;
        this.subscribeWebSocket();
      });
  }

  private subscribeWebSocket(): void {
    this.websocketService.messages?.pipe(takeUntil(this.destroyed$))
      .subscribe(msg => {
        console.log('Response from websocket: ' + msg);
        if (msg.body?.connected) {
          this.isConnectedWS = true;
          this.websocketService.sendPersonalData();
        } else if (msg.body.direction === 'Outbound') {
          const text = msg.body.text;
          const message: Message = {
            content: text,
            source: 'server'
          };
          this.messages.push(message);
          this.goToBottom();
          if (text === '#endchat') {
            this.websocketService.closeWS();
            this.chatEnded = true;
          }
        }
      });
  }


  private sendMessagegWebSocket(): void {
    const message: Message = {
      source: 'user',
      content: this.content
    };
    this.messages.push(message);
    this.goToBottom();
    this.websocketService.sendMessage(this.content);
    this.content = '';
  }

  private sendMessageBotMode(): void {
    if (this.sentBot.length === 0 && this.content === '1') {
      this.sentBot.push('1');
    }
    if (this.content.toLowerCase() === 'a') {
      this.sentBot.pop();
    } else {
      this.sentBot.push(this.content);
    }
    this.messages.push({ source: 'user', content: this.content });
    this.goToBottom();
    this.content = '';
    this.readWriteBot(this.sentBot)
      .subscribe(res => {
        this.messages.push(res.message);
        this.goToBottom();
      });
  }

  private goToBottom(): void {
    setTimeout(() => {
      const main = document.getElementsByTagName('main')[0];
      main.scrollTo({
        top: main.scrollHeight,
        behavior: 'smooth'
      });
    });
  }

  private closeChat(): void {
    // this.disconnect()
    //   .subscribe(() => {
    //     this.router.navigate(['/welcome-chat']);
    //   });
    this.sendEmail();
    this.websocketService.sendMessage('El cliente finalizó la conversación. Fin 👋');
    setTimeout(() => {
      this.websocketService.closeWS();
      this.storageSrv.removeSessionStorage('personalData');
      this.router.navigate(['/welcome-chat']);
    }, 1500);
  }

  private getBackendData(): void {
    this.authenticate()
      .pipe(
        mergeMap(res => {
          if (res.status === 401) {
            this.snackBar.open('Error de autenticación con el servidor TLS');
            return EMPTY;
          } else {
            return this.checkBotHealth();
          }
        }),
        mergeMap(res => {
          if (res.status !== 200) {
            this.snackBar.open('La conexión con el BOT no es estable');
            return EMPTY;
          } else {
            return this.getInitialConfiguration();
          }
        }),
        take(1)
      )
      .subscribe(res => {
        this.tabotIsConnected = true;
        this.messages = res.messages;
        this.goToBottom();
      });
  }

  private authenticate(): Observable<any> {
    return this.chatBotSrv.authenticate();
  }

  private disconnect(): Observable<any> {
    return this.chatBotSrv.disconnect();
  }

  private checkBotHealth(): Observable<any> {
    return this.chatBotSrv.checkHealthBot();
  }

  private getInitialConfiguration(): Observable<any> {
    return this.chatBotSrv.getInitialConfiguration();
  }

  private readWriteBot(input: string[]): Observable<any> {
    return this.chatBotSrv.readWriteBot(input);
  }

  private sendEmail(): void {
    if (this.checkSendEmail) {
      this.chatBotSrv.sendEmail(this.messages)
        .subscribe(res => {
          console.log(res);
        });
    }
  }
}
