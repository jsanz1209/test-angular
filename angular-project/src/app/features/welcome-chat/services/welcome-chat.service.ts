import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { countries } from 'src/app/collections/countries.collection';
import { identificationTypes } from 'src/app/collections/id-types.collection';

@Injectable({
  providedIn: 'root'
})
export class WelcomeChatService {


  getCountries(): Observable<any[]> {
    return of(countries);
  }

  getIdentificationTypes(): Observable<any[]> {
    return of(identificationTypes);
  }
}
