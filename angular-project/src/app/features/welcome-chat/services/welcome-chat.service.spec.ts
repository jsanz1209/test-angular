import { TestBed } from '@angular/core/testing';

import { WelcomeChatService } from './welcome-chat.service';

describe('WelcomeChatService', () => {
  let service: WelcomeChatService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(WelcomeChatService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
