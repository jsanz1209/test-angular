import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from 'src/app/core/modules/material.module';
import { WelcomeChatService } from './services/welcome-chat.service';
import { WelcomeChatRoutingModule } from './welcome-chat-routing/welcome-chat.routing.module';
import { WelcomeChatComponent } from './welcome-chat.component';

@NgModule({
    declarations: [
        WelcomeChatComponent
    ],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        MaterialModule,
        WelcomeChatRoutingModule
    ],
    providers: [
        WelcomeChatService
    ]
  })
  export class WelcomeChatModule {}
