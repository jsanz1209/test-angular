import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { Dropdown } from 'src/app/core/models/dropdown.model';
import { PersonalData } from 'src/app/core/models/personal-data.model';
import { FormValidatorsService } from 'src/app/core/services/form-validator.service';
import { FormService } from 'src/app/core/services/form.service';
import { StorageService } from 'src/app/core/services/storage.service';
import { WelcomeChatService } from './services/welcome-chat.service';
// import entire SDK
// import * as AWS from 'aws-sdk';

@Component({
  selector: 'app-welcome-chat',
  templateUrl: './welcome-chat.component.html',
  styleUrls: ['./welcome-chat.component.scss']
})
export class WelcomeChatComponent implements OnInit, OnDestroy {

  idTypes: Dropdown[] = [];
  countries: Dropdown[] = [];

  formGroup: FormGroup | undefined;

  private destroyed$ = new Subject<void>();

  constructor(private welcomeChatSrv: WelcomeChatService,
              private formValidatorsSrv: FormValidatorsService,
              private formSrv: FormService,
              private router: Router,
              private storageSrv: StorageService,
              private fb: FormBuilder) {
    this.initForm();
  }

  ngOnInit(): void {
    this.loadCountries();
    this.loadIdTypes();
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
    this.destroyed$.complete();
  }

  onCloseChat(): void {
    this.router.navigate(['/']);
  }

  onSubmit(): void {
    if (this.formGroup) {
      this.formSrv.updateFormStatus(this.formGroup);
      if (this.formGroup.valid) {
        const values = this.formGroup.value;
        this.sendPersonalData(values);
      }
    }
  }

  private initForm(): void {
    this.formGroup = this.fb.group({
      idType: ['', [Validators.required]],
      numId: ['', [Validators.required, this.formValidatorsSrv.validateNumID]],
      country: ['', [Validators.required]],
      name: ['', [Validators.required]],
      surname: ['', [Validators.required]],
      email: ['', [this.formValidatorsSrv.validateEmail, Validators.required]],
      phone: ['', [this.formValidatorsSrv.validatePhone, Validators.required]],
      explanation: ['', [Validators.required]],
      acceptPolicies: [null, [Validators.requiredTrue]]
    });
  }

  private loadCountries(): void {
    this.welcomeChatSrv.getCountries()
      .subscribe(countries => {
        this.countries = countries;
      });
  }

  private loadIdTypes(): void {
    this.welcomeChatSrv.getIdentificationTypes()
      .subscribe(idTypes => {
        this.idTypes = idTypes;
      });
  }

  private sendPersonalData(values: PersonalData): void {
    this.storageSrv.setSessionStorage('personalData', JSON.stringify(values));
    this.router.navigate(['/chat-bot']);
  }

}
