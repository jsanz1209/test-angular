import { Component } from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent {

  isMenuOpened = false;

  onToggleMenu(): void {
    this.isMenuOpened = !this.isMenuOpened;
  }
}
