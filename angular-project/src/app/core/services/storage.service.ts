import { Injectable, Inject, PLATFORM_ID } from '@angular/core';
import * as CryptoJS from 'crypto-js';
import { WINDOW } from '../tokens/window.token';
import { isPlatformBrowser } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  isActive = true;
  secretKey = 'SU9NUzIwMTk=';

  constructor(@Inject(WINDOW) private window: Window,
              @Inject(PLATFORM_ID) private platform: object) {
  }

  setLocalStorage(key: string, value: string): void {
    if (isPlatformBrowser(this.platform)) {
      if (this.isActive) {
        const descripted = (CryptoJS.AES.encrypt(JSON.stringify(value), this.secretKey) as unknown) as string;
        this.window.localStorage.setItem(key, descripted);
      } else {
        this.window.localStorage.setItem(key, JSON.stringify(value));
      }
    }
  }

  getLocalStorage(key: string): any {
    if (isPlatformBrowser(this.platform)) {
      if (this.isActive) {
        const ciphertext = (this.window.localStorage.getItem(key) as unknown) as string;
        if (ciphertext !== null) {
          try {
            const bytes = CryptoJS.AES.decrypt(ciphertext.toString(), this.secretKey);
            return JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
          } catch (error) {
            return JSON.parse(this.window.localStorage.getItem(key) as string);
          }
        }
        return null;
      } else {
        return JSON.parse(this.window.localStorage.getItem(key) as string);
      }
    }
    return null;
  }

  removeLocalStorage(key: string): void{
    if (isPlatformBrowser(this.platform) && this.window.localStorage.getItem(key)) {
      this.window.localStorage.removeItem(key);
    }
  }

  removeAllLocalStorage(): void {
    if (isPlatformBrowser(this.platform)) {
      this.window.localStorage.clear();
    }
  }


  setSessionStorage(key: string, value: string): void {
    if (isPlatformBrowser(this.platform)) {
      if (this.isActive) {
        const descripted = (CryptoJS.AES.encrypt(JSON.stringify(value), this.secretKey) as unknown) as string;
        this.window.sessionStorage.setItem(key, descripted);
      } else {
        this.window.sessionStorage.setItem(key, JSON.stringify(value));
      }
    }
  }

  getSessionStorage(key: string): any {
    if (isPlatformBrowser(this.platform)) {
      if (this.isActive) {
        const ciphertext = this.window.sessionStorage.getItem(key);
        if (ciphertext !== null) {
          try {
            const bytes = CryptoJS.AES.decrypt(ciphertext.toString(), this.secretKey);
            return JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
          } catch (error) {
            return JSON.parse(this.window.sessionStorage.getItem(key) as string);
          }
        }
        return null;
      } else {
        return JSON.parse(this.window.sessionStorage.getItem(key) as string);
      }
    }
    return null;
  }

  removeSessionStorage(key: string): void {
    if (isPlatformBrowser(this.platform) && this.window.sessionStorage.getItem(key)) {
      this.window.sessionStorage.removeItem(key);
    }
  }

  removeAllSessionStorage(): void {
    if (isPlatformBrowser(this.platform)) {
      this.window.sessionStorage.clear();
    }
  }
}
