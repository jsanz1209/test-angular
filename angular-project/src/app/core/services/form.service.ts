import { DOCUMENT } from '@angular/common';
import { Inject, Injectable } from '@angular/core';
import { FormArray, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FormService {

  private submit = new Subject<void>();
  submit$ = this.submit.asObservable();

  constructor(@Inject(DOCUMENT) private document: Document) { }

  updateFormStatus(form: FormGroup): void {
    Object.keys(form.controls).forEach(key => {
      form?.get(key)?.markAsTouched();
      form?.get(key)?.markAsDirty();
    });
  }

  updateFieldValidation(form: FormGroup | FormArray, field: string, validators: ValidatorFn[] | null = []): void {
    form.get(field)?.setValidators(validators);
    form.get(field)?.updateValueAndValidity();
  }

  updateFieldValue(form: FormGroup | FormArray, field: string, value?: string | null): void {
    if (typeof value === 'undefined') {
      value = '';
    }
    form.get(field)?.setValue(value);
    form.get(field)?.updateValueAndValidity({ onlySelf: false, emitEvent: true });
    form.get(field)?.markAsDirty();
  }

  resetFields(form: FormGroup, fields: string[] = []): void {
    fields.forEach(field => {
      form.get(field)?.markAsPristine();
      form.get(field)?.markAsUntouched();
      form.get(field)?.reset();
    });
  }

  removeValidatorsFields(form: FormGroup, fields: string[] = []): void {
    fields.forEach(field => {
      this.updateFieldValidation(form, field, null);
    });
  }

  addRequiredFields(form: FormGroup, fields: string[] = []): void {
    fields.forEach(field => {
      this.updateFieldValidation(form, field, [Validators.required]);
    });
  }
}
