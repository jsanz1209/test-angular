import { Injectable } from '@angular/core';
import { FormControl } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class FormValidatorsService {

  constructor() { }

  validateAllowedLetters = (field: FormControl) => {
    const lettersRexp = /^[a-zA-z-áéíóúÁÉÍÓÚ_üñÑçÇ ']+$/;
    const str = field.value !== null ? field.value.toString() : '';
    const lettersIsValid = str === '' ? true : lettersRexp.test(str);
    if (lettersIsValid) {
      return null;
    } else {
      return {
        pattern: {
          valid: false
        }
      };
    }
  }

  validateAllowedNumbers = (field: FormControl) => {
    const numbersRexp = /^[0-9]+$/;
    const str = field.value !== null ? field.value.toString() : '';
    const numbersIsValid = str === '' ? true : numbersRexp.test(str);
    if (numbersIsValid) {
      return null;
    } else {
      return {
        pattern: {
          valid: false
        }
      };
    }
  }

  validatePhone = (field: FormControl) => {
    const str = (field.value !== null ? field.value.toString() : '');
    const phoneIsValid = this.isValidPhone(str);
    if (phoneIsValid || !str.length) {
      return null;
    } else {
      return {
        pattern: {
          valid: false
        }
      };
    }
  }

  validateEmail = (field: FormControl) => {
    const str = field.value !== null ? field.value.toString() : '';
    const emailIsValid = this.isValidEmail(str);
    if (emailIsValid || !str.length) {
      return null;
    } else {
      return {
        email: {
          valid: false
        }
      };
    }
  }

  validateNumID = (field: FormControl) => {
    const str = field.value !== null ? field.value.toString() : '';
    const numIDIsValid = str.length >= 5;
    if (numIDIsValid || !str.length) {
      return null;
    } else {
      return {
        minlength: {
          valid: false
        }
      };
    }
  }

  isValidPhone(phone: string): boolean {
    const phoneRegExp = /^[0-9\-\+]{9,15}$/;
    return phoneRegExp.test(phone);
  }

  isValidEmail(email: string): boolean {
    const emailRexp = /^(([^<>()\\[\]\\.,;:\s@"]+(\.[^<>()\\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return emailRexp.test(email);
  }

}
