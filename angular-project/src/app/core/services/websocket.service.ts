import { Injectable } from '@angular/core';
import { Observable, Observer, ReplaySubject, Subject } from 'rxjs';
import { AnonymousSubject } from 'rxjs/internal/Subject';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { ReceivedMessage } from '../models/received-message.model';
import { StorageService } from './storage.service';


@Injectable()
export class WebsocketService {

    private ws: WebSocket | null = null;

    private subject: AnonymousSubject<MessageEvent> | undefined;
    public messages: Subject<any> | null = null;
    public connectionEstablished: ReplaySubject<void> | undefined = undefined;

    private identifier = '';

    constructor(private storageSrv: StorageService) { }

    requestConnection(): void {
        this.identifier = this.generateIdentifier();
        this.connectionEstablished = new ReplaySubject<void>();
        this.messages = (this.connect(`${environment.API_WS_URL}${environment.deploymentId}`).pipe(
            map(
                (response: MessageEvent): ReceivedMessage => {
                    console.log(response.data);
                    const data = JSON.parse(response.data);
                    return data;
                }
            )
        ) as Subject<ReceivedMessage>);
    }

    connect(url: string): AnonymousSubject<MessageEvent> {
        if (!this.subject) {
            this.subject = this.createWS(url);
            console.log('Successfully connected: ' + url);
            this.connectionEstablished?.next();
        }
        return this.subject;
    }

    sendMessage(text: string): void {
        this.messages?.next({
            action: 'onMessage',
            token: this.identifier,
            message: {
                type: 'Text',
                text
            }
        });
    }

    sendPersonalData(): void {
        const personalData = this.storageSrv.getSessionStorage('personalData');
        this.messages?.next({
            action: 'onMessage',
            token: this.identifier,
            message: {
                type: 'Text',
                text: 'User personal data'
            },
            channel: {
                metadata: {
                    customAttributes: {
                        ...JSON.parse(personalData)
                    }
                }
            }
        });
    }

    closeWS(): void {
        this.ws?.close();
        this.subject?.complete();
        this.subject?.unsubscribe();
        this.messages?.complete();
        this.messages?.unsubscribe();
        this.connectionEstablished?.complete();
        this.connectionEstablished?.unsubscribe();
        this.subject = undefined;
        this.connectionEstablished = undefined;
        this.ws = null;
        this.messages = null;
    }

    private generateIdentifier() {
        return ('[1e7]' + '-1e3' + '-4e3' + '-8e3' + '-1e11')
        .replace(/[018]/g, (c: string) => {
            // tslint:disable-next-line: no-bitwise
            return (Number(c) ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> Number(c) / 4).toString(16);
        });
    }

    private configureSession(): void {
        this.messages?.next({
            action: 'configureSession',
            deploymentId: environment.deploymentId,
            token: this.identifier
        });
    }

    private createWS(url: string): AnonymousSubject<MessageEvent> {
        this.ws = new WebSocket(url);
        const observable: Observable<MessageEvent<ReceivedMessage>> = new Observable((obs: Observer<MessageEvent>) => {
            if (this.ws !== null) {
                this.ws.onmessage = obs.next.bind(obs);
                this.ws.onopen = (() => {
                    this.configureSession();
                });
                this.ws.onerror = obs.error.bind(obs);
                this.ws.onclose = obs.complete.bind(obs);
            }
            return this.ws?.close.bind(this.ws);
        });
        const observer: Observer<MessageEvent<ReceivedMessage>> = {
            error: () => { },
            complete: () => { },
            next: (data: MessageEvent<ReceivedMessage>) => {
                console.log('Message sent to websocket: ', data);
                if (this.ws?.readyState === WebSocket.OPEN) {
                    this.ws.send(JSON.stringify(data));
                }
            }
        };

        return new AnonymousSubject<MessageEvent>(observer, observable);
    }
}
