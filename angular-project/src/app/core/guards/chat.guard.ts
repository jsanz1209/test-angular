import { Injectable } from '@angular/core';
import { CanActivate, CanLoad, Router } from '@angular/router';
import { StorageService } from '../services/storage.service';

@Injectable({
    providedIn: 'root'
})
export class ChatGuard implements CanActivate, CanLoad {

    constructor(private storageSrv: StorageService, private router: Router) { }

    canActivate(): boolean {
        return this.actionGuard();
    }

    canLoad(): boolean {
        return this.actionGuard();
    }

    private actionGuard(): boolean {
        const personaData = this.storageSrv.getSessionStorage('personalData');
        if (!personaData) {
            this.router.navigate(['/welcome-chat']);
        }
        return !!personaData;
    }
}


