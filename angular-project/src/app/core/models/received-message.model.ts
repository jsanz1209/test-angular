export interface ReceivedMessage {
    type: string;
    class: string;
    code: number;
    body: {
        text: string;
        direction: string;
        id: string;
        channel: {
            time: Date;
            type: string;
        };
        type: string;
        metadata: {
            [key: string]: string
        };
        originatingEntity: string;
    };
}