export interface Dropdown {
    code: string;
    name: string;
}
