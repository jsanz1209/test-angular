export interface PersonalData {
    idType: string;
    numId: string;
    country: string;
    name: string;
    surname: string;
    email: string;
    phone: string;
    explanation: string;
}
