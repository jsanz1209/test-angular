import { Dropdown } from '../core/models/dropdown.model';

export const identificationTypes: Dropdown[] = [
    {
        code: 'CC',
        name: 'Cédula de ciudadanía'
    },
    {
        code: 'CD',
        name: 'Carnet diplomático'
    },
    {
        code: 'CE',
        name: 'Cédula de extranjería'
    },
    {
        code: 'CC',
        name: 'Cédula de ciudadanía'
    },
    {
        code: 'IE',
        name: 'ID extranjero'
    },
    {
        code: 'FI',
        name: 'Fideicomiso'
    },
    {
        code: 'NIT',
        name: ''
    },
    {
        code: 'PA',
        name: 'Pasaporte'
    },
    {
        code: 'RC',
        name: 'Registro Civil'
    },
    {
        code: 'TI',
        name: 'Tarjeta de identidad'
    },
];
