import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ChatGuard } from './core/guards/chat.guard';

const routes: Routes = [
  {
    path: 'welcome-chat', loadChildren: () => import('./features/welcome-chat/welcome-chat.module').then(m => m.WelcomeChatModule) },
  { path: 'chat-bot', loadChildren: () => import('./features/chat-bot/chat-bot.module').then(m => m.ChatBotModule), canLoad: [ChatGuard] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
