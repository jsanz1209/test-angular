# Test-Angular

## Ejecutar el proyecto
Este repositorio consta de dos proyectos: Un proyecto **Angular** *(angular-project)* para la parte frontal y un proyecto **NodeJS + Express** *(ws-server-main)* para emular la API rest utilizada en el frontend.

Para ejecutar el workflow completo de la prueba es necesario realizar los siguientes pasos: 

Para levantar el **proyecto Angular** es necesario situarse en la ruta *angular-project* y descargarse previamente los paquetes de node mediante el comando **npm install**. Una vez acabado de ejecutarse la descarga de estos paquetes, ejecutaremos el comando **ng serve** para levantar el proyeco en local.

Para levantar el proyecto **NodeJS + Express** es necesario situarse en la ruta *ws-server-main* y descargarse previamente los paquetes de node mediante el comando **npm install**. Una vez acabado de ejecutarse la descarga de estos paquetes, ejecutaremos el comando **node index.js** para levantar el servidor Node que **emula** la **API REST** y en otra terminal ejecutaremos el comando **node websocket.js** para levantar el **servidor de Websocket**.

## Documentación proyecto Angular

Se componen de los siguientes directorios:

**app**: Punto de acceso a la aplicación Angular. Implementa el menú de acceso y el routing necesari para acceder a la pantalla de datos personales y el chat/BOT.

**components**: Directorio utilizado para almacenar los componentes utilizados en el componente principal de la aplicación, en este caso solo contiene un elemento que implementa el menú de acceso.

**core**: Es responsable de mantener los servicios globales como los servicios que gestionan y validan formularios, el servicio que se encarga de implementar el consumo del websocket en la app, los "guards" utilizados para impedir bloquear el routing cuando es necesario, o módulos básicos como los propios de Angular Material utilizados.

**features**: Contien los componentes y servidios necesarios para implementar el formulario de datos personales ***(welcome-chat)*** previo al acceso al acceso al chat y para la implementación del BOT, el chat a través de un agente y la evaluación de la experiencia del usuario con el chat. 

**(welcome-chat)**: Implementa el componente que muestra y gestiona el formulario de datos personales y consume las API REST necesarias para consultar los tipos de identificación ***(welcome.chat.component.ts)***, los países y enviar el formulario ***(welcome-chat.service)***.

**(chat-bot)**: Implementa el component que muestra y gestiona el BOT/CHAT ***(chat-bot.component.ts y chat-bot.service.ts)*** realizando la autenticación al servidor TLS, comprobando la salud del BOT, la configuración incial del BOT y la entrada/salidas de mensajes del mismo y también establece la conexión con el servidor Websocket para enviar mensajes y suscribirse a la entrada de mensajes por parte de un agente. La carpeta components contiene los componentes utilizados en el chat como los utilizados al mostrar los mensajes en pantalla, la encuesta y el sistema de puntuaciones de estrellas. 

## Documentación proyecto Node
El proyecto Node se ha utilizado para simular la API REST que se utiliza en la aplicación. En el se pueden distinguir los siguientes ficheros / directorios:

**security**: Contiene los certificados necesarios para emular la conexión SSL/TSL. Es posible que al ejecutar la aplicación tengáis que instalar el certificado **localhost.cer** en vuestra máquina o navegador para que podáis correr correctamente la API REST consumida en el proyecto frontend.

**bot-decision-tree**: Contiene la lógica necesaria para emular el árbol de decisión utilizado en el Bot.

**data**: Dummy data utilizada para las respuestas del endpoint que devuelve los paises y tipos de identificación

**middlewares**: Funciones utilizadas como middleware en el proyecto, como la utilizada para comprobar si la conexión con el servidor Node es segura, la función para generar el token de accesso o la función que comprueba si el usuario que está haciendo ciertas llamadas está autenticado en el servidor TLS

**index.js**: Contiene la API REST utilizada en el proyecto frontend. Se establece la conexión con el servidor SSL mediante un ejemplo de certificados que me he creado yo mismo. Destacan las funciones implemetadas para enviar los datos personales al servidor, para autenticarse en el servidor TLS mediante un token, las funciones para comprobar la salud del BOT e implementación de la lectura/escritura del mismo, así como también los servicios para enviar una encuesta.

**websocket.js**: Contiene la lógica necesaria para crear un servidor websocket para emular las conversaciones via chat en la aplicación

