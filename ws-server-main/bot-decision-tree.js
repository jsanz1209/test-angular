
const DecisionTree = require('decision-tree');

const initialBOTConfiguration = [
    {
        source: 'bot',
        content: `Somos un equipo experto que brinda asesoria a los clientes para tomar las mejores decisiones para acceder a los activos que facilitan su crecimiento y competitividad.`
    },
    {
        source: 'bot',
        content: 'Hola te, soy Tabot 🤖, tu asistente virtual de Bancolombia. Estoy aquí para darte una mano y guiarte a través de nuestros productos y servicios.'
    },
    {
        source: 'bot',
        content: `Nuestra forma de navegar ha cambiado. 😊 
        Ahora para seleccionar la opción que quieres consultar debes <strong>escribir el número correspondiente</strong>: 👇 
        <br/><br/>
        1. 💳 <strong>¡Nuevo!</strong> Productos Bancolombia 
        <br/>
        2. 🏦 Ubicar los puntos de atención cercanos
        <br/>
        3. 📱 Aprender a usar las plataformas digitales
        <br/><br/>
        👩&zwj;💻 Si lo necesitas, puedes escribir la palabra <strong>Asesor</strong> en <strong>cualquier momento</strong> para ser atendido por uno de ellos.`
    },
    {
        source: 'info',
        content: 'Tabot está conectado'
    }
];

const infoGenericChildrenMessages = `
<br/>
🔹 <strong>Recuerda que puedes:</strong> 
<em>Escribe una letra.</em> 
A. Regresar al menú anterior. 
 <br/>
🔹 Escribe <strong>Asesor</strong> para ser atendido por uno de ellos.`;

const bot_data = [
    {
        level: "1",
        message: {
            source: 'bot',
            content: `1. 💳 <strong>¡Nuevo!</strong> Productos Bancolombia',<br/>
            2. 🏦 Ubicar los puntos de atención cercanos,<br/>
            3. 📱 Aprender a usar las plataformas digitales,<br/>
            ${infoGenericChildrenMessages}`
        }
    },
    {
        level: "11",
        message: {
            source: 'bot',
            content: `
            1. 💰 Cuentas bancarias,<br/>
            2. 📲 Bancolombia A la mano,<br/>
            3. 💳 Tarjetas de crédito,<br/>
            4. 📈 Inversiones,<br/>
            ${infoGenericChildrenMessages}`
        }
    },
    { level: "12", message: { source: 'bot', content: `1) Ubicar los puntos de atención cercanos, <br/>2) Desplazarse a través de la aplicación de mapas<br/> ${infoGenericChildrenMessages}` } },
    { level: "13", message: { source: 'bot', content: `1) Aprender a usar las plataformas digitales,<br/>2) Solicitar acceso<br/> ${infoGenericChildrenMessages}` } },
    { level: "113", message: { source: 'bot', content: `1) Text 1 of level 1.1.3,<br/>2) Text 2 of level 1.1.3<br/>${infoGenericChildrenMessages}` } },
    { level: "2", message: { source: 'bot', content: `1) Text 1 of level 2,<br/>2) Text 2 of level 2<br/>${infoGenericChildrenMessages}` } },
    { level: "21", message: { source: 'bot', content: `1) Text 1 of level 2.1,<br/>2) Text 2 of level 2.1<br/>${infoGenericChildrenMessages}` } },
    { level: "213", message: { source: 'bot', content: `1) Text 1 of level 2.1.3,<br/>2) Text 2 of level 2.1.3<br/>${infoGenericChildrenMessages}` } },
];

var class_name = "message";

var features = ['level'];

var dt = new DecisionTree(bot_data, class_name, features);

function getTextLevels(level) {
    return dt.predict({
        level
    });
}

module.exports = {
    initialBOTConfiguration: initialBOTConfiguration,
    getTextLevels: getTextLevels
};