const express = require('express');
const ws = require('ws');
const helmet = require('helmet');
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());
app.use(helmet()); // Add Helmet as a middleware

const port = 5000;
let indexMessage = 0;
const messages = [
    { source: 'info', content: 'Tabot está desconectado' },
    { source: 'info', content: 'Un asesor estará en breve contigo' },
    { source: 'info', content: 'jhoier.vega conectado' },
    { source: 'server', content: 'Buenos días , bienvenido a Bancolombia, nos alegra que visite nuestro canal virtual, ¿Cómo está el día de hoy?, es importante que tengas habilitadas las ventanas emergentes (pop ups) para que te lleguen notificaciones de nuestros mensajes en el chat. Esta opción la puedes encontrar en la configuración del navegador' },
    { source: 'server', content: 'Response from agent' },
    { source: 'server', content: 'Response from agent' },
    { source: 'server', content: 'Response from agent' },
    { source: 'server', content: 'Response from agent' },
    { source: 'server', content: 'Response from agent' },
    { source: 'server', content: '¿Se encuentra en línea?' },
    { source: 'server', content: 'Debo finalizar la interacción ya que usted no continúa en línea, esperamos que vuelva a utilizar nuestro canal para tener el gusto de asesorarte en sus inquietudes. Le deseo un feliz día.' },
    { source: 'info', content: 'jhoier.vega desconectado' },
    { source: 'info', content: 'Chat finalizado' },
];

// Set up a headless websocket server that prints any
// events that come in.
const wsServer = new ws.Server({ noServer: true });
wsServer.on('connection', socket => {
    indexMessage = 0;
    socket.on('message', message => {
        wsServer.clients.forEach(function each(client) {
            if (client.readyState === ws.OPEN) {
                setTimeout(function () {

                    if (indexMessage === 0) {
                        client.send(
                            Buffer.from(JSON.stringify(messages[indexMessage]))
                            , { binary: false });

                        indexMessage++;

                        client.send(
                            Buffer.from(JSON.stringify(messages[indexMessage]))
                            , { binary: false });

                        indexMessage++;

                        client.send(
                            Buffer.from(JSON.stringify(messages[indexMessage]))
                            , { binary: false });

                        indexMessage++;

                        client.send(
                            Buffer.from(JSON.stringify(messages[indexMessage]))
                            , { binary: false });

                        indexMessage++;

                    } else if (indexMessage === messages.length - 3) {

                        client.send(
                            Buffer.from(JSON.stringify(messages[indexMessage]))
                            , { binary: false });

                        indexMessage++;

                        client.send(
                            Buffer.from(JSON.stringify(messages[indexMessage]))
                            , { binary: false });

                        indexMessage++;

                        client.send(
                            Buffer.from(JSON.stringify(messages[indexMessage]))
                            , { binary: false });


                    } else {
                        client.send(
                            Buffer.from(JSON.stringify(messages[indexMessage]))
                            , { binary: false });
                    }

                    if (indexMessage < messages.length) {
                        indexMessage++;
                    }

                }, 1000);
            }
        });
        console.log(message.toString());
    }
    );
});

// `server` is a vanilla Node.js HTTP server, so use
// the same ws upgrade process described here:
// https://www.npmjs.com/package/ws#multiple-servers-sharing-a-single-https-server
const server = app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
});

server.on('upgrade', (request, socket, head) => {
    wsServer.handleUpgrade(request, socket, head, socket => {
        wsServer.emit('connection', socket, request);
    });
});