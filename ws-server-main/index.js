const express = require('express'),
    ws = require('ws'),
    bodyParser = require('body-parser'),
    https = require('https'),
    fs = require('fs'),
    helmet = require("helmet"),
    cors = require('cors'),
    jwt = require('jsonwebtoken'),
    dotenv = require('dotenv');

const botDecisionTree = require('./bot-decision-tree');
const data = require('./data');
const middlewares = require('./middlewares');

// get config vars
dotenv.config();

const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());
if (process.env.NODE_ENV !== 'development') {
    app.use(helmet()); // Add Helmet as a middleware
}

const port = 4000;

const httpsOptions = {
    key: fs.readFileSync('./security/cert.key'),
    cert: fs.readFileSync('./security/cert.pem')
}

// redirect any page form http to https
app.use((req, res, next) => {
    if (!middlewares.isSecure(req)) {
        console.log('insecure, redirecting...');
        res.redirect(301, `https://localhost:${port}${req.url}`);
    } else {
        console.log('secure');
        next();
    }
});

app.get('/', (req, res) => {
    res.send('<a href="/tls/authenticate">Log in using client certificate</a>')
});

app.post('/tls/authenticate', (req, res) => {
    const authorization = req.headers.authorization;
    const token = authorization.replace('Basic ', '');
    if (token === process.env.TOKEN_AUTH_TL) {
        const accessToken = middlewares.generateAccessToken({ username: 'admin' });
        res.status(200).send({
            status: 200, 
            chatID: 342343432,
            userID: 54323,
            accessToken: accessToken, message: 'Authenticated successfully'
        });
    } else {
        return res.status(401).send({ status: 401, message: 'Not Authorized' });
    }
});

app.get('/tls/check-health-bot', middlewares.authenticateToken, (req, res) => {
    return res.status(200).send({ status: 200, message: 'The health of BOT is good' });
});

app.get('/tls/get-bot-configuration', middlewares.authenticateToken, (req, res) => {
    return res.status(200).send({
        status: 200,
        messages: botDecisionTree.initialBOTConfiguration
    });
});

app.post('/tls/disconnect', middlewares.authenticateToken, (req, res) => {
    const { chatID, userID} = req.body;

    if(!chatID || !userID) {
        return res.status(400).send({
            status: 400,
            message: 'Bad request to logout'
        }); 
    }

    return res.status(200).send({
        status: 200,
        message: 'Logout on Server TLS'
    });
});

app.post('/tls/read-write-bot', middlewares.authenticateToken, (req, res) => {
    const { arrayLevels } = req.body;
    if (!arrayLevels) {
        return res.status(400).send({ status: 400, message: 'Bad Request, it\'s necessary indicate the levels' })
    } else {
        const levels = arrayLevels.toString().replace(/,/g, '');
        const message = botDecisionTree.getTextLevels(levels);
        return res.status(200).send({ status: 200, message: message });
    }
});

app.post('/tls/generate-evaluation', middlewares.authenticateToken, (req, res) => {
    const { personalData, userID, chatID, typeAgent } = req.body;

    if (!personalData || !userID || !chatID) {
        return res.status(400).send({status: 400, message: 'Bad request generating evaluation'});
    } else {
        return res.status(200).send({status: 200, message: 'Evaluation generated successfully', evaluationID: 43242323, typeAgent, personalData, userID, chatID});
    }
});

app.post('/tls/add-evaluation', middlewares.authenticateToken, (req, res) => {
    const { evaluationID, responses } = req.body;

    if (!evaluationID || !responses) {
        return res.status(400).send({status: 400, message: 'Bad request adding evaluation'});
    } else {
        return res.status(200).send({status: 200, message: 'Evaluation added successfully', evaluationID, responses});
    }
});

app.post('/send-email', function (req, res) {
    const { messages } = req.body;
    if (messages) {
        return res.status(200).send({ status: 200, messages: messages });
    } else {
        return res.status(400).send({ status: 400, message: 'Bad request' });
    }
});

app.post('/personal-data', function (req, res) {
    const allIsFulfilled = Object.keys(req.body).every(key => !!req.body[key]);
    if (allIsFulfilled) {
        const response = {
            error: false,
            status: 200,
            message: 'Data was sended successfully'
        };
        res.status(200).send(response);
    } else {
        return res.status(400).send({
            error: true,
            status: 400,
            message: 'Bad request, missing information in the request'
        })
    }
});

app.get('/countries', function (req, res) {
    res.status(200).send(data.countries);
});

app.get('/identification-types', function (req, res) {
    res.status(200).send(data.identificationTypes);
});

https.createServer(httpsOptions, app).listen(port, () => {
    console.log('server running at https://localhost:' + port)
})
