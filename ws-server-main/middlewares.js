const jwt = require('jsonwebtoken');

/**
 * @param req express http request
 * @returns true if the http request is secure (comes form https)
 */
 function isSecure(req) {
    if (req.headers['x-forwarded-proto']) {
        return req.headers['x-forwarded-proto'] === 'https';
    }
    return req.secure;
}

function generateAccessToken(username) {
    return jwt.sign(username, process.env.TOKEN_SECRET, { expiresIn: '1800s' });
}

function authenticateToken(req, res, next) {
    const authHeader = req.headers['authorization'];
    const token = authHeader && authHeader.split(' ')[1];

    if (token == null) return res.sendStatus(401);

    jwt.verify(token, process.env.TOKEN_SECRET, (err, user) => {
        console.log(err);
        if (err) return res.sendStatus(403);
        req.user = user;
        next();
    });
}

module.exports = {
    isSecure: isSecure,
    generateAccessToken: generateAccessToken,
    authenticateToken: authenticateToken
};